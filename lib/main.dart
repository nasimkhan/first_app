import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title: const Text(
              'Project ABC',
              style: TextStyle(fontSize: 20, color: Colors.black),
            ),
            leading: const Icon(
              Icons.arrow_back_outlined,
              size: 24,
              color: Colors.white,
            ),
            actions: const [
              Icon(Icons.account_circle),
            ],
          ),
          body: Center(
            child: Image.network(
              'https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg',
              height: 400,
              width: 350,
            ),
          ),
          floatingActionButton: const Icon(Icons.add),
        ),
      ),
      theme: ThemeData.light(),
    );
  }
}
